
# Install nginx
ansible-playbook -i inventory/dev/hosts install-nginx.yml --ask-become-pass

# Uninstall nginx
ansible-playbook -i inventory/dev/hosts uninstall-nginx.yml --ask-become-pass

